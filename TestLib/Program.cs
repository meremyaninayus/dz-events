﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using EventsDZ;
using Microsoft.Extensions.Configuration;

namespace TestLib
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            string targetDirectory = configuration["targetDirectory"];
            var fileNamesNodes = configuration
                .GetChildren()
                .Where((x) => x.Key == "fileNames")
                .FirstOrDefault()
                .GetChildren();
            var fileNamesArray = new string[fileNamesNodes.Count()];
            for (int i=0; i< fileNamesNodes.Count(); i++)
            {
                fileNamesArray[i] = fileNamesNodes.ElementAt(i).Value;
            }    
            if (!int.TryParse(configuration["timeOut"], out var timeOut))
            {
                return;
            }
            var receiver = new DocumentsReceiver(fileNamesArray.ToList());
            receiver.Start(targetDirectory, timeOut);
            receiver.DocumentsReady += NotifyGood;
            receiver.TimedOut += NotifyTimeOut;
            Console.WriteLine("Hello!");
            Thread.Sleep(3000);
            Console.ReadKey();
        }
        static void NotifyGood()
        {
            Console.WriteLine("All done");
        }
        static void NotifyTimeOut()
        {
            Console.WriteLine("TimeOut");
        }
    }
}
