﻿using System;
using System.IO;
using System.Timers;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace EventsDZ
{
    public class DocumentsReceiver:IDisposable
    {
        private Dictionary<string, bool> _filesInfo;
        private FileSystemWatcher _watcher;
        private Timer _timer;
        private bool _disposedValue;

        public DocumentsReceiver(List<string> fileNames)
        {
            _filesInfo = new Dictionary<string, bool>();
            foreach (var fileName in fileNames)
            {
                _filesInfo.Add(fileName.ToLower(), false);
            }
        }

        public delegate void DocumentsReceiverHadler();
        public delegate void DocumentsReceiverTimeOutHadler();
        public event DocumentsReceiverHadler DocumentsReady;
        public event DocumentsReceiverTimeOutHadler TimedOut;

        public void Start(string targetDirectory, int timerInterval)
        {
            _watcher = new FileSystemWatcher(targetDirectory);
            _watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.CreationTime;
            _watcher.Created += CheckDocuments;
            _watcher.EnableRaisingEvents = true;

            _timer = new Timer();
            _timer.Start();
            _timer.Interval = timerInterval;
            _timer.Elapsed += ElapsTimer;
        }

        private void ElapsTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            _watcher.Created -= CheckDocuments;
            _timer.Elapsed -= ElapsTimer;
            TimedOut();
        }

        private void CheckDocuments(object sender, FileSystemEventArgs e)
        {
            if (_filesInfo.ContainsKey(e.Name.ToLower()))
            {
                _filesInfo[e.Name.ToLower()] = true;
            }

            if (!_filesInfo.ContainsValue(false))
            {
                _watcher.Created -= CheckDocuments;
                _timer.Elapsed -= ElapsTimer;
                DocumentsReady();
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }
                _watcher.Dispose();
                _timer.Dispose();
                // TODO: set large fields to null
                _disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
         ~DocumentsReceiver()
         {
             // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
             Dispose(disposing: false);
         }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
